import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Quote } from '../../data/quote.interface';
import { QuotesService } from '../../services/quotes';


@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage implements OnInit{

  quotes: {category:string, quotes:Quote[], icon:string}[];
  words:{quotes:Quote};
  constructor(private alertCtrl: AlertController, private navParams: NavParams, private quotesService:QuotesService) {

  }

  ngOnInit(){
    this.quotes = this.navParams.data;
    this.words = this.navParams.data.quotes;
    //console.log(this.quotesService);
    //console.log(this.quotes);
    //console.log(this.navParams.data.quotes);
  }
  
  onAddQuote(quote:Quote){
    this.quotesService.addQuoteToFavorites(quote);
  }

  onRemoveQuote(quote:Quote){
    this.quotesService.removeQuoteFromFavorites(quote);
  }

  onShowAlert(quote:Quote){
    const alert = this.alertCtrl.create({
      title: "Add Quote?",
      subTitle: "",
      message: "Are you sure want to add quotes to favorite?",
      buttons:[
        {
          text: "Ok",
          handler:()=>{
              //console.log(quote.text);
              this.onAddQuote(quote);
              console.log(this.quotesService);
             
          }
        },
        {
          text: "Cancel",
          role:'cancel',
          handler:()=>{
              console.log("No");
          }
        }
      ]
    });
    alert.present();
  }

  onShowAlertUnfavorite(quote:Quote){
    const alert = this.alertCtrl.create({
      title: "Remove from Favorite Quote?",
      subTitle: "",
      message: "Are you sure want to remote quotes from favorite?",
      buttons:[
        {
          text: "Ok",
          handler:()=>{
              //console.log(quote.text);
              this.onRemoveQuote(quote);
              console.log(this.quotesService);
              //this.ngOnInit();
          }
        },
        {
          text: "Cancel",
          role:'cancel',
          handler:()=>{
              console.log("No");
          }
        }
      ]
    });
    alert.present();
  }

  isFavorite(quote:Quote){
    return this.quotesService.isFavorite(quote);
  }
}
