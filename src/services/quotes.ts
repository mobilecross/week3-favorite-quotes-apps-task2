import { Quote } from '../data/quote.interface';

export class QuotesService{
    private favoriteQuotes: Quote[]=[];

    addQuoteToFavorites(quote:Quote){
        this.favoriteQuotes.push(quote);
    }

    removeQuoteFromFavorites(quote: Quote){
        this.favoriteQuotes.splice(this.favoriteQuotes.indexOf(quote),1);
    }

    getFavoriteQuotes(){

    }

    isFavorite(quote: Quote){
        return this.favoriteQuotes.indexOf(quote) >= 0;
    }
}